// Arduino code to measure frequency of a signal
// and send the value to another board over uart.
// Current status: abandoned.

#include <FreqCount.h>
#include <FreqMeasure.h>
#include <avr/io.h>
#define F_CPU 16000000
#define BAUD 9600
#include<util/setbaud.h>

void UART_Init();

void setup()
{
  Serial.begin(9600, SERIAL_8O1);
  pinMode(0, INPUT);
  pinMode(1, OUTPUT);
  //UART_Init();

  //FreqCount.begin(1000);
  FreqMeasure.begin();

  delay(5000);
}

double sum = 0;
int count = 0;

void loop()
{
  //Serial.write(65); 
  //delay(4000);
  if (FreqMeasure.available())
  {
    FreqMeasure.end();
    //Serial.println("# of samples available");
    //Serial.println(FreqMeasure.available());
    //FreqMeasure.countToFrequency(
    long thing = FreqMeasure.read();
    sum = sum + thing;
    count = count + 1;
    //Serial.println(thing);
    if (count > 30)
    {
      float freq = FreqMeasure.countToFrequency(sum / count);

      sum = 0;
      count = 0;
      int twoFiveSixes = freq / 255;
      //Serial.write(twoFiveSixes);
      
      unsigned int ifreq = freq;
      unsigned int remainder = ifreq % 255;
      //Serial.println(count);
      //Serial.println(freq);
      Serial.print(twoFiveSixes);
      delay(2000);
      Serial.write(remainder);
      delay(2000);
    }
    FreqMeasure.begin();

    //Serial.write(remainder);


    //count = count - 1000;
    //UART_Transmit(count);
  }
  //delay(1000);
}


void UART_Init()
{
  UBRR0H = UBRRH_VALUE;
  UBRR0L = UBRRL_VALUE;
  UCSR0B = (1 << RXEN0) | (1 << TXEN0);
  UCSR0C = (3 << UCSZ00);
}

void UART_Transmit(unsigned char data)
{
  while (!(UCSR0A & (1 << UDRE0)));
  UDR0 = data;
}
