// Arduino code to count frequency of a signal
// and send the value to another board over uart.
// Current status: abandoned.

#include <FreqCount.h>
#include <avr/io.h>
#define F_CPU 16000000
#define BAUD 9600 
#include<util/setbaud.h>

void UART_Init();

void setup()
{
  Serial.begin(9600, SERIAL_8O1);
  pinMode(0,INPUT);
  pinMode(1,OUTPUT);
  //UART_Init();
  
  //FreqCount.begin(1000);
  FreqMeasure.begin();
  
  delay(12000);
}

void loop()
{
  //Serial.write(65);
  //delay(4000);
  if (FreqCount.available())
  {
    unsigned long count = FreqCount.read();
    int twoFiveSixes = count / 255;
    Serial.write(twoFiveSixes);
    delay(200);
    unsigned int remainder = count % 255;
    Serial.write(remainder);

    Serial.println(count);
    Serial.println(twoFiveSixes);
    Serial.println(remainder);
    //count = count - 1000;
    //UART_Transmit(count);
  }
  delay(4000); 
}
 

void UART_Init()
{
   UBRR0H = UBRRH_VALUE;
   UBRR0L = UBRRL_VALUE; 
   UCSR0B = (1<<RXEN0) | (1<<TXEN0);
   UCSR0C = (3<<UCSZ00);
}

void UART_Transmit(unsigned char data)
{
    while(!(UCSR0A & (1<<UDRE0)));
    UDR0 = data;
}
