/* File: main.h
 * Author: Grant Capan
 */

#include "msp.h"
#include "stdint.h"
#include "math.h"
#include "pwm.h"

void enable_interrupts(void);
