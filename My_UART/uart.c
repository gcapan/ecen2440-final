/*
 * uart.c
 *
 *  Created on: Nov 17, 2019
 *      Author: Grant Capan
 */
#include "msp.h"

#define SYSCLK      3000000;
#define BAUD_RATE   9600;

void init_UART()
{

    /* Begin Port Mapping */
    PMAP->KEYID = PMAP_KEYID_VAL; // Grants access to all port mapping configuration registers.
    PMAP->CTL = PMAP_CTL_PRECFG;    // Enable reconfiguration of port mappings.

    //Configure pins for UART
    P6->SEL0 |= BIT6 | BIT7;
    P6->SEL1 |= BIT6 | BIT7;
    //P6->SEL0 = 0;
    P6->SEL1 = 0;

    P6->DIR &= ~BIT6;
    P6->DIR |= BIT7;
    //Map ports to use pins 6.6 and 6.7 as our UART TXD & RXD pins.
    P6MAP->PMAP_REGISTER6 = PM_UCA0RXD; // Map Port 6, Pin 6 to eUSCI_A0 UART RXD
    P6MAP->PMAP_REGISTER7 = PM_UCA0TXD; // Map Port 6, Pin 7 to eUSCI_A0 UART TXD
    PMAP->KEYID = 0;
    /* End Port Mapping */

    // UART: 8-N-1
    // 8 bits, no parity bit, 1 stop bit
    /* Begin configuring UART */
    EUSCI_A2->CTLW0 |= EUSCI_A_CTLW0_SWRST; // Set reset bit to disable UART during configuration
    EUSCI_A2->CTLW0 &= ~EUSCI_A_CTLW0_SYNC; // Select UART mode

    /* Calculating baud rate according to TRM page 915
     * N = fBRCLK / (baud rate) = 3000000 / 9600 = 312.5
     * So,
     * OS16 = 1, UCBRx = INT(N/16), UCBRFx = INT([(N/16) - INT(N/16)] * 16)
     * OS16 = 1, UCBRx = 312, UCBRFx = 8
     */
    EUSCI_A0->BRW = 312;
    EUSCI_A0->MCTLW |= (8 << EUSCI_A_MCTLW_BRF_OFS) | EUSCI_A_MCTLW_OS16; // Configure MCTLW register for oversampling mode with a value of 8 in the BRF section

    EUSCI_A2->CTLW0 |= EUSCI_A_CTLW0_SSEL__SMCLK; // Keeping EUSCI_A0 in reset, configure eUSCI CLK source to SMCLK
    EUSCI_A2->CTLW0 &= ~UC7BIT;
    EUSCI_A2->CTLW0 &= ~UCPEN;
    EUSCI_A2->CTLW0 &= ~EUSCI_A_CTLW0_MODE_MASK;
    /* Calculating baud rate according to TRM page 915
     * N = fBRCLK / (baud rate) = 3000000 / 9600 = 312.5
     * So,
     * OS16 = 1, UCBRx = INT(N/16), UCBRFx = INT([(N/16) - INT(N/16)] * 16)
     * OS16 = 1, UCBRx = 312, UCBRFx = 8
     */
    EUSCI_A2->BRW = 78;
    EUSCI_A2->MCTLW = (2 << EUSCI_A_MCTLW_BRF_OFS) | EUSCI_A_MCTLW_OS16;

    P3->DIR |= BIT3;
    P3->DIR &= ~BIT2;
    P3->OUT |= BIT3;
    P3->SEL0 |= BIT2 | BIT3;
    P3->SEL1 = 0;

    EUSCI_A2->CTLW0 &= ~EUSCI_A_CTLW0_SWRST;   // Clear reset bit to enable UART
    EUSCI_A2->IFG &= ~EUSCI_A_IFG_RXIFG; // Clear RX IFG (gets set when the reset bit is set)
    EUSCI_A2->IE &= EUSCI_A_IE_RXIE;            // Disable RX interrupts

    //EUSCI_A0->MCTLW |= UCOS16; // enable oversampling mode

    /* Stop configuring UART */

}

