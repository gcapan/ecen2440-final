#include "main.h"
//#include "stdint.h"

/*License: freeware, you can modify, distribute this file freely
 * Digole Digital Solutions: www.digole.com
 * Glossary:
 * Position and Pixel position: the position is counted as pixels, start from 0.
 * Character position: counted based on selected font's size, start from0, the accurate position as pixels are depends on the font,
 * eg.: character position (1,1) for font=0, equal pixel position (7,19)
 * 
 * Even there were lot of functions in lib, but you can browse the DigoleSerial.c file, then you will find the functions are very simple
 * because the Digole's onboard MCU will handle the complicated jobs.
 * If any one want to transplant this lib to other programming language, feel free to do it, it's not too hard for all
 * 
 * -------IMPORTANT-------
 * You must write two functions for your host MCU before using this C lib:
 * unsigned char Digole_write(unsigned char value); //write a byte to serial port (UART/I2C/SPI)
 * unsigned char Digole_read(void); //read a byte from serial port
 */
#ifndef DigoleSerialDisp_h
#define DigoleSerialDisp_h
// Communication set up command
// Text function command
// Graph function command
#define Serial_UART 0
#define Serial_I2C 1
#define Serial_SPI 2
#define _TEXT_ 0
#define _GRAPH_ 1
#ifdef FLASH_CHIP   //if writing to flash chip
#define _WRITE_DELAY 40
#else   //if writing to internal flash
#define _WRITE_DELAY 40
#endif
#ifndef Ver
#define Ver 39
#endif
//#define FLASH_CHIP
//if you didn't define a communication mode, use UART as default
//#include "i2c_hardware.h"

/*-----Basic functions----*/
//write one byte or 2 bytes value to serial port, depends on the value of v, if v<255, write one by, others write 255, then v-255.
//we use this function to make code compatible on small (<255) size screens and large (>=255) screens
void Digole_write2B(unsigned int v);

/*---------fucntions for Text and Graphic LCD adapters---------*/
void Digole_printStr(const char *s);    //print text string on the screen
void Digole_writeStr(const char *s);    //write non-zero string to the serial port
void Digole_disableCursor(void);    //disable flashing cursor
void Digole_enableCursor(void);     //enable flashing cursor
void Digole_clearScreen(void);      //clear whole screen area by using background color
//assign a new I2C address to the display, the display will remember the new address and use it
void Digole_setI2CAddress(unsigned char add);
void Digole_displayConfig(unsigned char v);   //set the showing of serial port config on/off, 1=on, 0=off
//print function

void Digole_preprint(void); //write "TT" to display to let the following data display on the screen
void Digole_printChar(char a);  //print a character on screen
/*----------Functions for Graphic LCD/OLED adapters only---------*/
//the functions in this section compatible with u8glib
//draw mono image using current forecolor, each byte represent 8 pixels
void Digole_drawBitmap(unsigned int x, unsigned int y, unsigned int w, unsigned int h, const unsigned char *bitmap);
//draw 8bit format color image, each byte represent a pixel, the format is:RRRGGGBB
void Digole_drawBitmap8bit(unsigned int x, unsigned int y, unsigned int w, unsigned int h, const unsigned char *bitmap);
//draw 16bit format color image, 2 bytes represent a pixel, the format is: RRRRRGGG,GGGBBBBB
void Digole_drawBitmap16bit(unsigned int x, unsigned int y, unsigned int w, unsigned int h, const unsigned char *bitmap);
//draw 18bit format color image, 3 bytes represent a pixel, the format is: 00RRRRRR,00GGGGGG,00BBBBBB
void Digole_drawBitmap18bit(unsigned int x, unsigned int y, unsigned int w, unsigned int h, const unsigned char *bitmap);
//set forecolor, the color format is 18bit: 00XXXXXX
void Digole_setTrueColor(unsigned char r, unsigned char g, unsigned char b);
//set the screen rotation, only affect the following output on screen
void Digole_setRot90(void);
void Digole_setRot180(void);
void Digole_setRot270(void);
//set the screen rotation to 0 degree
void Digole_undoRotation(void);
//another way to set screen rotation, the value 0=0 degree, 1=90 degree...
void Digole_setRotation(unsigned char);
//set screen contrast, only use able on some mono display
void Digole_setContrast(unsigned char);
//draw a filled rectangle by using forecolor
void Digole_drawBox(unsigned int x, unsigned int y, unsigned int w, unsigned int h);
//draw a filled/non-filled circle,using forecolor, f=1 filled, f=0 non-filled
void Digole_drawCircle(unsigned int x, unsigned int y, unsigned int r, unsigned char f);
//void Digole_drawDisc(unsigned int x, unsigned int y, unsigned int r);
//draw a non-filled rectangle,using forecolor
void Digole_drawFrame(unsigned int x, unsigned int y, unsigned int w, unsigned int h);
//draw a pixel on the screen using forecolor
void Digole_drawPixel(unsigned int x, unsigned int y);
//draw a line from (x,y) to (x1,y1) by using forecolor
void Digole_drawLine(unsigned int x, unsigned int y, unsigned int x1, unsigned int y1);
//draw a line from current position to (x,y) by using forecolor
void Digole_drawLineTo(unsigned int x, unsigned int y);
//draw a horizontal line from left(x,y) width is w, by using forecolor
void Digole_drawHLine(unsigned int x, unsigned int y, unsigned int w);
//draw a vertical line from top(x,y), height is h, by using forecolor
void Digole_drawVLine(unsigned int x, unsigned int y, unsigned int h);
//-------------------------------
//special functions for our adapters
//void uploadStartScreen(int lon, const unsigned char *data); //upload start screen
void Digole_setFont(unsigned char font); //set font, availale: 6,10,18,51,120,123, user font: 200-203
void Digole_nextTextLine(void); //got to next text line, depending on the font size
void Digole_setColor(unsigned char); //set color for graphic function
void Digole_backLightOn(void); //Turn on back light
void Digole_backLightOff(void); //Turn off back light
void Digole_screenOnOff(unsigned char); //turn screen on/off
void Digole_cpuOff(void); //put MCU in sleep, it will wake up when new data received
void Digole_moduleOff(void); //put whole module in sleep: Back light off, screen in sleep, MCU in sleep
void Digole_backLightBrightness(unsigned char); //set backlight brightness,0~100

void Digole_directCommand(unsigned char d); //send command to LCD drectly
void Digole_directData(unsigned char d); //send data to LCD drectly
//move a area of screen to another place
void Digole_moveArea(unsigned int x0, unsigned int y0, unsigned int w, unsigned int h, char xoffset, char yoffset);
//display text string s on the screen at character position (x,y)
void Digole_drawStr(unsigned int x, unsigned int y, const char *s);
//set current print position on screen, if m=0, set (x,y) as character position, otherwise, set as pixel position
void Digole_setPrintPos(unsigned int x, unsigned int y, unsigned char m);
//for Digole character display or adapter only, set the display columns and rows that the adapter works with
void Digole_setLCDColRow(unsigned char col, unsigned char row);
//set the current print position based on pixels, same as:Digole_setPrintPos(x,y,1)
void Digole_setTextPosAbs(unsigned int x, unsigned int y);
/*-----Touch screen functions---*/
//re-calibrate the touch panel
void Digole_calibrateTouchScreen(void);
//wait and read touched position, then store it to x,y
void Digole_readTouchScreen(int *x,int *y);
//not wait, read touch screen position, and store it to x,y. if not touched, the x=-1;
void Digole_readTouchScreenInst(int *x,int *y);
//wait touch screen pressed then released (a click event), then store the position to x,y
void Digole_readClick(int *x,int *y);
//read the voltage input on pin Vbat, voltage=(read value)*2 mv
int Digole_readBattery(void);
//read the signal input on AUX pin
int Digole_readAux(void);
//read the chip temperature: the degree in census=(653-result*2500/4096)/2.1
int Digole_readTemperature(void);

/*-----Flash memory functions---*/
//erase partial of flash memory for store data later
void Digole_flashErase(unsigned long int addr, unsigned long int length);
//write data stored in code space to flash chip
void Digole_flashWrite_const(unsigned long int addr, unsigned long int len, const unsigned char *data);
//write data stored in RAM to flash chip
void Digole_flashWrite(unsigned long int addr, unsigned long int len, unsigned char *data);
//initial a start reading data from flash chip, then use Digole_read() function to read all "len" bytes of data
void Digole_flashReadStart(unsigned long int addr, unsigned long int len);
//use the data saved in flash chip at address "addr" as current font
void Digole_setFlashFont(unsigned long int addr);
//use the data saved in flash chip at address "addr" as serial input till an extra value of 255, instead of the data from serial port
void Digole_runCommandSet(unsigned long int addr);

/*--- new function on V3.3 firmware ----*/
//write len bytes of data to E2PROM, E2PROM has about 10x life time than flash on re-writing cycles, 976 bytes of E2PROM for user
void Digole_writeE2prom(unsigned int addr, unsigned int len, unsigned char *data);
//initial a start reading data from E2PROM, then use Digole_read() function to read all "len" bytes of data
void Digole_readE2prom(unsigned int addr, unsigned int len);

/*--- new function on V3.0 firmware and later ----*/
//set background color, 8bit format:RRRGGGBB
void Digole_setBgColor(unsigned char color);
//set a partial area of screen as output window
void Digole_setDrawWindow(unsigned int x, unsigned int y, unsigned int width, unsigned int height);
//set full screen area as output window
void Digole_resetDrawWindow(void);
//clear the draw window by using background color
void Digole_cleanDrawWindow(void);
//---end of V3.0 functions

/*-----Others----*/
//turn on(m=1)/off(m=0) splash screen when power on
void Digole_displayStartScreen(unsigned char m);
//set logical mode for on-comming output to screen, m='C'-Copy, '|'-Or, '!'-Not, '^'-Xor, '&'-And
void Digole_setMode(unsigned char m);
//move the character position back to last character, only one can be used
void Digole_setTextPosBack(void);
//move current position to (x+xoffset,y+yoffset), the offset can be positive or negative
void Digole_setTextPosOffset(char xoffset, char yoffset);
//set the pattern when draw line, one byte for pattern, use this, can draw dot line, dash line and more.
void Digole_setLinePattern(unsigned char pattern);
//this function only for Universal 12864 GLCD adapter to set the adapter work with ST7920,ST7565 and KS0108
void Digole_setLCDChip(unsigned char chip);
//set LCD backlight brightness, value from 0 to 100
void Digole_setBackLight(unsigned char bl);
//only for some mono display, to output x to pins
void Digole_digitalOutput(unsigned char x);
//for mono screen only, to enforce the MCU flush out the internal screen data in RAM to screen to avoid screen flicking
//when bl=1, the MCU automatically flush the new data in RAM to screen when no new commands in buffer, when bl=0, the MCU
//will waiting next flash screen command then flash the RAM to screen
void Digole_flushScreen(unsigned char bl);
//download the splash screen data to onboard flash memory
void Digole_downloadStartScreen(int lon, const unsigned char *data);
//download user font's data to onboard flash memory, if no-flash chip, the data is saved on MCU, and only 4 user fonts space available.
//if flash chip installed, the user font can be saved any where in chip by using Digole_flashWrite() or Digole_flashWrite_const(),
//then use Digole_setFlashFont() to use it, and the number of user fonts are unlimited.
void Digole_downloadUserFont(int lon, const unsigned char *data, unsigned char sect);
//send c as screen command directly to LCD, you must know very well on the LCD drive chip
void Digole_manualCommand(unsigned char c);
//send d value as screen data directly to LCD, you must know very well on the LCD drive chip
void Digole_manualData(unsigned char d);
//The SPI have mode 0,1,2,3, varies on Clock polarity and phase
void Digole_setSPIMode(unsigned char mode);
//read a integer value from display module, the previous command must a command that module will return data
int Digole_readInt(void);
#endif
