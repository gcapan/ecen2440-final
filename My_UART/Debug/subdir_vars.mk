################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../msp432p401r.cmd 

C_SRCS += \
../DigoleSerial.c \
../main.c \
../startup_msp432p401r_ccs.c \
../system_msp432p401r.c \
../uart.c 

C_DEPS += \
./DigoleSerial.d \
./main.d \
./startup_msp432p401r_ccs.d \
./system_msp432p401r.d \
./uart.d 

OBJS += \
./DigoleSerial.obj \
./main.obj \
./startup_msp432p401r_ccs.obj \
./system_msp432p401r.obj \
./uart.obj 

OBJS__QUOTED += \
"DigoleSerial.obj" \
"main.obj" \
"startup_msp432p401r_ccs.obj" \
"system_msp432p401r.obj" \
"uart.obj" 

C_DEPS__QUOTED += \
"DigoleSerial.d" \
"main.d" \
"startup_msp432p401r_ccs.d" \
"system_msp432p401r.d" \
"uart.d" 

C_SRCS__QUOTED += \
"../DigoleSerial.c" \
"../main.c" \
"../startup_msp432p401r_ccs.c" \
"../system_msp432p401r.c" \
"../uart.c" 


