/*
 * uart.h
 *
 *  Created on: Nov 17, 2019
 *      Author: grant
 */

#ifndef UART_H_
#define UART_H_

void init_UART(); // Function to initialize and configure UART
/* UART using a BAUD rate of 9600, derived from the 3MHz default System Clock
 * Port mapping used to forward TXD and RXD to pins 6.6 and 6.7.
 */



#endif /* UART_H_ */
