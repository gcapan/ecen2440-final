#include "main.h"

/**
 * main.c
 * https://www.embeddedrelated.com/showarticle/420.php
 */


void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer

    init_UART();
    int i;
	while(1)
	{
	    Digole_printStr("This is a test of the Digole whatever screen.  I am writing a line of text.");
	    Digole_delay(_WRITE_DELAY);
	    Digole_clearScreen();
	}
}
