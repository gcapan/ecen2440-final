#include "main.h"

/**
 * File: main.c
 * Author: Grant Capan
 * Guided by:
 * https://www.embeddedrelated.com/showarticle/420.php
 */


#define SYSTEM_CLOCK        3000000
#define BAUD_RATE           9600

uint8_t recData;
uint8_t RxBufSize = 0;
uint8_t RxByte0;

void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer
    int i;
    for (i = 0; i < 100000; i++);
    init_OLED_UART();

    //init_Arduino_UART();
    // ^-- The Arduino is no longer used, uncomment this line to use it again.

    char theChar = 'a';

    bootAnim();

    enable_Interrupts();

    while (1)
    {
        Digole_setPrintPos(0,55,1);
        Digole_printStr("Final Proj.");
        Digole_delay(10000);
        Digole_setPrintPos(0,65,1);
        Digole_printStr("Grant Capan");
        Digole_delay(10000);

        Digole_setPrintPos(0,65,1);
        Digole_printStr("Justin Jiang");
        Digole_delay(10000);

        Digole_setPrintPos(0,65,1);
        Digole_printStr("                     ");
        Digole_setPrintPos(0,65,1);
        Digole_printStr("Noah Saidy");
        Digole_delay(10000);
    }

    Digole_clearScreen();

    Digole_delay(10000);
}

void bootAnim()
{
    Digole_setDrawWindow(0, 0, 96, 96);
    Digole_clearScreen();
    Digole_delay(_WRITE_DELAY);
    Digole_enableCursor();
    Digole_delay(7000);
    Digole_printStr("Welcome.");
    Digole_delay(7000);
    Digole_disableCursor();
    Digole_clearScreen();
    Digole_drawCircle(60, 55, 5, 0);
    Digole_drawCircle(50, 55, 5, 0);
    Digole_drawCircle(40, 55, 5, 0);
    Digole_drawCircle(30, 55, 5, 0);
    Digole_drawCircle(55, 45, 5, 0);
    Digole_drawCircle(45, 45, 5, 0);
    Digole_drawCircle(35, 45, 5, 0);
    Digole_delay(10000);
    Digole_moveArea(25, 40, 45, 25, 0, -5);
    Digole_delay(_WRITE_DELAY);
    Digole_moveArea(25, 35, 45, 25, 0, -5);
    Digole_delay(_WRITE_DELAY);
    Digole_moveArea(25, 30, 45, 25, 0, -5);
    Digole_delay(_WRITE_DELAY);
    Digole_moveArea(25, 25, 45, 25, 0, -5);
    Digole_delay(1000);
    Digole_setPrintPos(0,0,0);
    Digole_printStr("ECEN2440");
    Digole_delay(5000);

    Digole_setDrawWindow(0, 65, 96, 31);

    Digole_enableCursor();
}


void setRxInt()
{
    P9->SEL0 |= BIT6;
    P9->REN &= ~BIT6;
    EUSCI_A3->IFG &= ~EUSCI_A_IFG_RXIFG;
    EUSCI_A3->IE |= EUSCI_A_IE_RXIE;
}

void enable_Interrupts()
{
    NVIC->ISER[0] = 1 << ((EUSCIA3_IRQn) & 31);
    setRxInt();

    PCM->CTL1 = PCM_CTL0_KEY_VAL | PCM_CTL1_FORCE_LPM_ENTRY;

    __enable_irq();         // Overall IRQ functionality enable
    __low_power_mode_off_on_exit();

    __DSB();                // Enable Data Synchronization Barrier

}

void EUSCIA3_IRQHandler()
{
    if(EUSCI_A3->IFG & EUSCI_A_IFG_RXIFG)
    {
        recData = EUSCI_A3->RXBUF;
        if(RxBufSize == 0)
            RxByte0 = recData;
        RxBufSize = RxBufSize + 1;

        if(RxBufSize >= 2)
        {
            RxBufSize = 0;
            int digit = RxByte0 - '0';
            int TwoFiftySixes = 255 * digit;
            int result = TwoFiftySixes + (recData - '0');
            //result = result / 4;

            Digole_setPrintPos(12, 60, 0);
            char TxDataChar[15];
            itoa(result, TxDataChar);
            Digole_printStr(TxDataChar);
            Digole_delay(_WRITE_DELAY);
            Digole_printStr(" Hz");
            Digole_delay(2000);
        }

        EUSCI_A3->IFG &= ~EUSCI_A_IFG_RXIFG;
    }
}



// itoa and reverse functions source:
// https://en.wikibooks.org/wiki/C_Programming/stdlib.h/itoa

/* itoa:  convert n to characters in s */
void itoa(int n, char s[])
{
    int i, sign;

    if ((sign = n) < 0)  /* record sign */
        n = -n;          /* make n positive */
    i = 0;
    do {       /* generate digits in reverse order */
        s[i++] = n % 10 + '0';   /* get next digit */
    } while ((n /= 10) > 0);     /* delete it */
    if (sign < 0)
        s[i++] = '-';
    s[i] = '\0';
    reverse(s);
}

/* reverse:  reverse string s in place */
void reverse(char s[])
{
    int i, j;
    char c;

    for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}
