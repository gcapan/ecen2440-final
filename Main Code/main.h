/*
 * main.h
 *
 *  Created on: Nov 17, 2019
 *      Author: grant
 */

#ifndef MAIN_H_
#define MAIN_H_

#include "msp.h"
#include "DigoleSerial.h"
//#include "stdint.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

void bootAnim();

void setP9_6Int();

void setRxInt();

void enable_Interrupts();

void itoa(int n, char s[]);
void reverse(char s[]);

#endif /* MAIN_H_ */
