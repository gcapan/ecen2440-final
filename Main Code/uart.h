/*
 * uart.h
 *
 *  Created on: Nov 17, 2019
 *      Author: grant
 */

#ifndef UART_H_
#define UART_H_

void init_OLED_UART(); // Function to initialize and configure UART to drive the OLED

void clock_Config();        // Function to set up the SYSCLK
                            // to make a 9600 Baud Rate more attainable.

void uart_OLED_GPIO();
void uart_Arduino_GPIO();           // Function to configure UART pins.

void init_Arduino_UART();   // Function to init UART to receive frequency data from Arduino.



#endif /* UART_H_ */
