//License: freeware, you can modify, distribute this file freely
//Digole Digital Solutions: www.digole.com
#include "DigoleSerial.h"
//extern unsigned char Digole_write(unsigned char value); //write a byte to serial port (UART/I2C/SPI)
//extern unsigned char Digole_read(void); //read a byte from serial port

unsigned char Digole_write(unsigned char value)
{
    EUSCI_A2->TXBUF = value;
    Digole_delay(_WRITE_DELAY);
    return value;
}

unsigned char Digole_read(void)
{
    return EUSCI_A2->RXBUF;
}


void Digole_delay(int a) {
    int i;
    char j;
    for (i = 0; i < a; i++)
        for (j = 0; j < 250; j++)
            ;
}

void Digole_write2B(unsigned int v) {
    if (v < 255)
        Digole_write(v);
    else {
        Digole_write(255);
        Digole_write(v - 255);
    }
}

void Digole_writeStr(const char *s) {
    while (*s != 0){
        Digole_write(*s);
        s++;
    }
}

void Digole_printStr(const char *s) {
//    Digole_writeStr("TT");
    Digole_preprint();
    Digole_writeStr(s);
    Digole_write(0);
}

void Digole_printChar(char a) {
//    Digole_writeStr("TT");
    Digole_preprint();
    Digole_write(a);
    Digole_write(0);
}

void Digole_disableCursor(void) {
    Digole_writeStr("CS0");
}

void Digole_enableCursor(void) {
    Digole_writeStr("CS1");
}

void Digole_clearScreen(void) {
    Digole_writeStr("CL");
}

void Digole_setI2CAddress(unsigned char add) {
    Digole_writeStr("SI2CA");
    Digole_write(add);
    //        _I2Caddress = add;
}

void Digole_displayConfig(unsigned char v) {
    Digole_writeStr("DC");
    Digole_write(v);
}

void Digole_preprint(void) {
    //write((unsigned char)0);
    Digole_writeStr("TT");
}

/*----------Functions for Graphic LCD/OLED adapters only---------*/
void Digole_drawBitmap(unsigned int x, unsigned int y, unsigned int w, unsigned int h, const unsigned char *bitmap) {
    unsigned char i = 0;
    if ((w & 7) != 0)
        i = 1;
    Digole_writeStr("DIM");
    Digole_write2B(x); //x;
    Digole_write2B(y);
    Digole_write2B(w);
    Digole_write2B(h);
    int j;
    for (j = 0; j < h * ((w >> 3) + i); j++) {
        Digole_write(*(bitmap + j));
        //        delay(5);
    }
}

void Digole_setRot90(void) {
    Digole_writeStr("SD1");
}

void Digole_setRot180(void) {
    Digole_writeStr("SD2");
}

void Digole_setRot270(void) {
    Digole_writeStr("SD3");
}

void Digole_undoRotation(void) {
    Digole_writeStr("SD0");
}

void Digole_setRotation(unsigned char d) {
    Digole_writeStr("SD");
    Digole_write2B(d);
}

void Digole_setContrast(unsigned char c) {
    Digole_writeStr("CT");
    Digole_write2B(c);
}

void Digole_drawBox(unsigned int x, unsigned int y, unsigned int w, unsigned int h) {
    Digole_writeStr("FR");
    Digole_write2B(x);
    Digole_write2B(y);
    Digole_write2B(x + w);
    Digole_write2B(y + h);
}

void Digole_drawCircle(unsigned int x, unsigned int y, unsigned int r, unsigned char f) {
    Digole_writeStr("CC");
    Digole_write2B(x);
    Digole_write2B(y);
    Digole_write2B(r);
    Digole_write(f);
}

void Digole_drawDisc(unsigned int x, unsigned int y, unsigned int r) {
    Digole_drawCircle(x, y, r, 1);
}

void Digole_drawFrame(unsigned int x, unsigned int y, unsigned int w, unsigned int h) {
    Digole_writeStr("DR");
    Digole_write2B(x);
    Digole_write2B(y);
    Digole_write2B(x + w);
    Digole_write2B(y + h);
}

void Digole_drawPixel(unsigned int x, unsigned int y) {
    Digole_writeStr("DP");
    Digole_write2B(x);
    Digole_write2B(y);
}

void Digole_drawLine(unsigned int x, unsigned int y, unsigned int x1, unsigned int y1) {
    Digole_writeStr("LN");
    Digole_write2B(x);
    Digole_write2B(y);
    Digole_write2B(x1);
    Digole_write2B(y1);
}

void Digole_drawLineTo(unsigned int x, unsigned int y) {
    Digole_writeStr("LT");
    Digole_write2B(x);
    Digole_write2B(y);
}

void Digole_drawHLine(unsigned int x, unsigned int y, unsigned int w) {
    Digole_drawLine(x, y, x + w, y);
}

void Digole_drawVLine(unsigned int x, unsigned int y, unsigned int h) {
    Digole_drawLine(x, y, x, y + h);
}

void Digole_nextTextLine(void) {
    Digole_write((unsigned char) 0);
    Digole_writeStr("TRT");
}

void Digole_setFont(unsigned char font) {
    Digole_writeStr("SF");
    Digole_write(font);
}

void Digole_setColor(unsigned char color) {
    Digole_writeStr("SC");
    Digole_write(color);
}

void Digole_directCommand(unsigned char d) {
    Digole_writeStr("MCD");
    Digole_write(d);
}

void Digole_directData(unsigned char d) {
    Digole_writeStr("MDT");
    Digole_write(d);
}

void Digole_moveArea(unsigned int x0, unsigned int y0, unsigned int w, unsigned int h, char xoffset, char yoffset) {
    Digole_writeStr("MA");
    Digole_write2B(x0);
    Digole_write2B(y0);
    Digole_write2B(w);
    Digole_write2B(h);
    Digole_write(xoffset);
    Digole_write(yoffset);
}

void Digole_drawBitmap8bit(unsigned int x, unsigned int y, unsigned int w, unsigned int h, const unsigned char *bitmap) { //display 256 color image
    Digole_writeStr("EDIM1");
    Digole_write2B(x); //x;
    Digole_write2B(y);
    Digole_write2B(w);
    Digole_write2B(h);
    int j;
    for (j = 0; j < h * w; j++) {
        Digole_write(*(bitmap + j));
    }
}

void Digole_drawBitmap16bit(unsigned int x, unsigned int y, unsigned int w, unsigned int h, const unsigned char *bitmap) { //display 256 color image
    Digole_writeStr("EDIM2");
    Digole_write2B(x); //x;
    Digole_write2B(y);
    Digole_write2B(w);
    Digole_write2B(h);
    int j;
    for (j = 0; (j < h * w * 2); j++) {
        Digole_write(*(bitmap + j));
    }
}

void Digole_drawBitmap18bit(unsigned int x, unsigned int y, unsigned int w, unsigned int h, const unsigned char *bitmap) { //display 256 color image
    Digole_writeStr("EDIM3");
    Digole_write2B(x); //x;
    Digole_write2B(y);
    Digole_write2B(w);
    Digole_write2B(h);
    int j;
    for (j = 0; (j < h * w * 3); j++) {
        Digole_write(*(bitmap + j));
    }
}

void Digole_setTrueColor(unsigned char r, unsigned char g, unsigned char b) { //Set true color
    Digole_writeStr("ESC");
    Digole_write(r);
    Digole_write(g);
    Digole_write(b);
}

void Digole_drawStr(unsigned int x, unsigned int y, const char *s) {
    Digole_writeStr("TP");
    Digole_write2B(x);
    Digole_write2B(y);
    //Digole_writeStr("TT");
    Digole_preprint();
    Digole_writeStr(s);
    Digole_write((unsigned char) 0);
}

void Digole_setPrintPos(unsigned int x, unsigned int y, unsigned char graph) {
    if (graph == 0) {
        Digole_writeStr("TP");
        Digole_write2B(x);
        Digole_write2B(y);
    } else {
        Digole_writeStr("GP");
        Digole_write2B(x);
        Digole_write2B(y);
    }
}

void Digole_setLCDColRow(unsigned char col, unsigned char row) {
    Digole_writeStr("STCR");
    Digole_write(col);
    Digole_write(row);
    Digole_writeStr("\x80\xC0\x94\xD4");
}

void Digole_setTextPosAbs(unsigned int x, unsigned int y) {
    Digole_writeStr("ETP");
    Digole_write2B(x);
    Digole_write2B(y);
}

//----Touch screen functions-----//

void Digole_calibrateTouchScreen(void) {
    Digole_writeStr("TUCHC");
}
void Digole_readTouchScreenInst(int *x,int *y){
    Digole_writeStr("RPNXYI");
    *x = Digole_readInt();
    *y = Digole_readInt();
}

void Digole_readTouchScreen(int *x, int *y) {
    Digole_writeStr("RPNXYW");
    *x = Digole_readInt();
    *y = Digole_readInt();
}

void Digole_readClick(int *x, int *y) //read a click on touch screen
{
    Digole_writeStr("RPNXYC");
    *x = Digole_readInt();
    *y = Digole_readInt();
}

int Digole_readBattery(void) {
    int c;
    Digole_writeStr("RDBAT");
    c = Digole_readInt();
    return c;
}

int Digole_readAux(void) {
    int c;
    Digole_writeStr("RDAUX");
    c = Digole_readInt();
    return c;
}

int Digole_readTemperature(void) {
    int c;
    Digole_writeStr("RDTMP");
    c = Digole_readInt();
    return c;
}
//-----Flash memory functions----//

void Digole_flashErase(unsigned long int addr, unsigned long int length) {
    Digole_writeStr("FLMER");
    Digole_write(addr >> 16);
    Digole_write(addr >> 8);
    Digole_write(addr);
    Digole_write(length >> 16);
    Digole_write(length >> 8);
    Digole_write(length);
}

void Digole_flashReadStart(unsigned long int addr, unsigned long int len) {
    Digole_writeStr("FLMRD");
    Digole_write(addr >> 16);
    Digole_write(addr >> 8);
    Digole_write(addr);
    Digole_write(len >> 16);
    Digole_write(len >> 8);
    Digole_write(len);
}

void Digole_setFlashFont(unsigned long int addr) {
    Digole_writeStr("SFF");
    Digole_write(addr >> 16);
    Digole_write(addr >> 8);
    Digole_write(addr);
}

void Digole_runCommandSet(unsigned long int addr) {
    Digole_writeStr("FLMCS");
    Digole_write(addr >> 16);
    Digole_write(addr >> 8);
    Digole_write(addr);
}
//V3.3 functions

void Digole_writeE2prom(unsigned int addr, unsigned int len, unsigned char *data) {
    unsigned char c;
    Digole_writeStr("WREP");
    Digole_write(addr >> 8);
    Digole_write(addr);
    Digole_write(len >> 8);
    Digole_write(len);
    int i;
    for (i = 0; i < len; i++) {
        c = data[i];
        Digole_write(c);
    }
}

void Digole_readE2prom(unsigned int addr, unsigned int len) {
    Digole_writeStr("RDEP");
    Digole_write(addr >> 8);
    Digole_write(addr);
    Digole_write(len >> 8);
    Digole_write(len);
}

void Digole_backLightOn(void) {
    Digole_writeStr("BL");
    Digole_write((unsigned char) 99);
}

void Digole_backLightOff(void) {
    Digole_writeStr("BL");
    Digole_write((unsigned char) 0);
}

void Digole_screenOnOff(unsigned char a) //turn screen on/off
{
    Digole_writeStr("SOO");
    Digole_write((unsigned char) a);
}

void Digole_cpuOff(void) //put MCU in sleep, it will wake up when new data received
{
    Digole_writeStr("DNMCU");
}

void Digole_moduleOff(void) //put whole module in sleep: Back light off, screen in sleep, MCU in sleep
{
    Digole_writeStr("DNALL");
}

void Digole_backLightBrightness(unsigned char a) //set backlight brightness,0~100
{
    Digole_writeStr("BL");
    Digole_write((unsigned char) a);
}
//--- new function on V3.0 firmware ----//

void Digole_setBgColor(unsigned char color) //set current color as background
{

#if Ver>32
    Digole_writeStr("BGC");
    Digole_write(color);
#else
    Digole_writeStr("SC");
    Digole_write(color);
    Digole_writeStr("FTOB");
#endif
}

void Digole_setDrawWindow(unsigned int x, unsigned int y, unsigned int width, unsigned int height) {
    Digole_writeStr("DWWIN");
    Digole_write2B(x);
    Digole_write2B(y);
    Digole_write2B(width);
    Digole_write2B(height);
}

void Digole_resetDrawWindow(void) {
    Digole_writeStr("RSTDW");
}

void Digole_cleanDrawWindow(void) {
    Digole_writeStr("WINCL");
}
//---end of V3.0 functions

void Digole_displayStartScreen(unsigned char m) {
    Digole_writeStr("DSS");
    Digole_write(m);
} //display start screen

void Digole_setMode(unsigned char m) {
    Digole_writeStr("DM");
    Digole_write(m);
} //set display mode

void Digole_setTextPosBack(void) {
    Digole_writeStr("ETB");
} //set text position back to previous, only one back allowed

void Digole_setTextPosOffset(char xoffset, char yoffset) {
    Digole_writeStr("ETO");
    Digole_write(xoffset);
    Digole_write(yoffset);
}

void Digole_setLinePattern(unsigned char pattern) {
    Digole_writeStr("SLP");
    Digole_write(pattern);
}

void Digole_setLCDChip(unsigned char chip) { //only for universal LCD adapter
    Digole_writeStr("SLCD");
    Digole_write(chip);
}

void Digole_setBackLight(unsigned char bl) {
    Digole_writeStr("BL");
    Digole_write(bl);
}

void Digole_digitalOutput(unsigned char x) {
    Digole_writeStr("DOUT");
    Digole_write(x);
}

void Digole_flushScreen(unsigned char bl) {
    Digole_writeStr("FS");
    Digole_write(bl);
}

void Digole_downloadStartScreen(int lon, const unsigned char *data) {
    int j;
    unsigned char b;
    unsigned char c;
    Digole_writeStr("SSS");
    lon++;
    Digole_write((unsigned char) (lon / 256));
    Digole_write((unsigned char) (lon % 256));
    Digole_delay(300);
    b = 0;
    for (j = 0; j < (lon - 1); j++) {
        c = *(data + j);
        Digole_write(c);
        if ((++b) == 64) {
            b = 0, Digole_delay(_WRITE_DELAY);
        }
    }
    Digole_write(255); //indicater of end of it
    Digole_delay(_WRITE_DELAY);
}

void Digole_downloadUserFont(int lon, const unsigned char *data, unsigned char sect) {
    unsigned char c;
    unsigned char b;
    Digole_writeStr("SUF");
    Digole_write(sect);
#if Ver>32
    Digole_write((unsigned char) (lon / 256));
    Digole_write((unsigned char) (lon % 256));
#else
    Digole_write((unsigned char) (lon % 256));
    Digole_write((unsigned char) (lon / 256));
#endif
    b = 0;
    int j;
    for (j = 0; j < lon; j++) {
        c = *(data + j);
        Digole_write(c);
        if ((++b) == 64) {
            b = 0, Digole_delay(_WRITE_DELAY);
        }
    }
}

void Digole_flashWrite_const(unsigned long int addr, unsigned long int len, const unsigned char *data) {
    unsigned char c, b;
    unsigned long int i;
    Digole_writeStr("FLMWR");
    Digole_write(addr >> 16);
    Digole_write(addr >> 8);
    Digole_write(addr);
    Digole_write(len >> 16);
    Digole_write(len >> 8);
    Digole_write(len);
    b = 0;
    for (i = 0; i < len; i++) {
        c = *(data + i);
        Digole_write(c);
        if ((++b) == 64) {
            b = 0, Digole_delay(_WRITE_DELAY);
        }
    }
#ifdef FLASH_CHIP
    //check write memory done
    while (Digole_read() != 17);
#endif
}

void Digole_flashWrite(unsigned long int addr, unsigned long int len, unsigned char *data) {
    unsigned char c, b;
    unsigned long int i;
    Digole_writeStr("FLMWR");
    Digole_write(addr >> 16);
    Digole_write(addr >> 8);
    Digole_write(addr);
    Digole_write(len >> 16);
    Digole_write(len >> 8);
    Digole_write(len);
    b = 0;
    for (i = 0; i < len; i++) {
        c = data[i];
        Digole_write(c);
        if ((++b) == 64) {
            b = 0, Digole_delay(_WRITE_DELAY);
        }
    }
#ifdef FLASH_CHIP
    //check write memory done
    while (Digole_read() != 17);
#endif
}

void Digole_manualCommand(unsigned char c) {
    Digole_writeStr("MCD");
    Digole_write(c);
}

void Digole_manualData(unsigned char d) {
    Digole_writeStr("MDT");
    Digole_write(d);
}

void Digole_setSPIMode(unsigned char mode) {
    if (mode >= 0 && mode < 4) {
        Digole_writeStr("SPIMD");
        Digole_write(mode);
    }
}

int Digole_readInt(void) {
    int i = Digole_read() << 8;
    i += Digole_read();
    return i;
}
