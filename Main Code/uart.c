/*
 * uart.c
 *
 *  Created on: Nov 17, 2019
 *      Author: Grant Capan
 */
#include "uart.h"
#include "msp.h"

void init_OLED_UART()
{
    clock_Config();         // Configure SYSCLK to make BR=9600 more attainable.

    uart_OLED_GPIO();            // Configure UART Pins.

    // UART: 8-N-1
    // 8 bits, no parity bit, 1 stop bit
    /* Begin configuring UART */
    EUSCI_A2->CTLW0 |= EUSCI_A_CTLW0_SWRST; // Set reset bit to disable UART during configuration
    EUSCI_A2->CTLW0 &= ~EUSCI_A_CTLW0_SYNC; // Select UART mode

    EUSCI_A2->CTLW0 |= EUSCI_A_CTLW0_SSEL__SMCLK; // Keeping EUSCI_A0 in reset, configure eUSCI CLK source to SMCLK
    EUSCI_A2->CTLW0 &= ~UC7BIT;
    EUSCI_A2->CTLW0 &= ~UCPEN;
    EUSCI_A2->CTLW0 &= ~EUSCI_A_CTLW0_MODE_MASK;
    /* Calculating baud rate according to TRM page 915
     * N = fBRCLK / (baud rate) = 3000000 / 9600 = 312.5
     * So,
     * OS16 = 1, UCBRx = INT(N/16), UCBRFx = INT([(N/16) - INT(N/16)] * 16)
     * OS16 = 1, UCBRx = 312, UCBRFx = 8
     */
    EUSCI_A2->BRW = 78;
    EUSCI_A2->MCTLW = (2 << EUSCI_A_MCTLW_BRF_OFS) | EUSCI_A_MCTLW_OS16;

    EUSCI_A2->CTLW0 &= ~EUSCI_A_CTLW0_SWRST;   // Clear reset bit to enable UART
    EUSCI_A2->IFG &= ~EUSCI_A_IFG_RXIFG; // Clear RX IFG (gets set when the reset bit is set)
    EUSCI_A2->IE &= EUSCI_A_IE_RXIE;            // Disable RX interrupts

    //EUSCI_A0->MCTLW |= UCOS16; // enable oversampling mode

    /* Stop configuring UART */
}

void init_Arduino_UART()
{
    uart_Arduino_GPIO();

    // UART: 8-N-1
    // 8 bits, no parity bit, 1 stop bit
    /* Begin configuring UART */
    EUSCI_A3->CTLW0 |= EUSCI_A_CTLW0_SWRST; // Set reset bit to disable UART during configuration
    EUSCI_A3->CTLW0 &= ~EUSCI_A_CTLW0_SYNC; // Select UART mode

    EUSCI_A3->CTLW0 |= EUSCI_A_CTLW0_SSEL__SMCLK; // Keeping EUSCI_A0 in reset, configure eUSCI CLK source to SMCLK
    EUSCI_A3->CTLW0 &= ~UC7BIT;
    EUSCI_A3->CTLW0 &= ~UCPEN;
    EUSCI_A3->CTLW0 &= ~EUSCI_A_CTLW0_MODE_MASK;
    /* Calculating baud rate according to TRM page 915
     * N = fBRCLK / (baud rate) = 3000000 / 9600 = 312.5
     * So,
     * OS16 = 1, UCBRx = INT(N/16), UCBRFx = INT([(N/16) - INT(N/16)] * 16)
     * OS16 = 1, UCBRx = 312, UCBRFx = 8
     */
    EUSCI_A3->BRW = 78;
    EUSCI_A3->MCTLW = (2 << EUSCI_A_MCTLW_BRF_OFS) | EUSCI_A_MCTLW_OS16;

    EUSCI_A3->CTLW0 &= ~EUSCI_A_CTLW0_SWRST;   // Clear reset bit to enable UART
    EUSCI_A3->IFG &= ~EUSCI_A_IFG_RXIFG; // Clear RX IFG (gets set when the reset bit is set)
    EUSCI_A3->IE |= EUSCI_A_IE_RXIE;            // Enable RX interrupts



    /* Stop configuring UART */
}

void clock_Config()
{
    // Clock configuration borrowed from TI examples, this function (C) Texas Instruments.
    CS->KEY = CS_KEY_VAL;                // Unlock CS module for register access
    CS->CTL0 = 0;                           // Reset tuning parameters
    CS->CTL0 = CS_CTL0_DCORSEL_3; // Set DCO to 12MHz (nominal, center of 8-16MHz range)
    CS->CTL1 = CS_CTL1_SELA_2 |             // Select ACLK = REFO
            CS_CTL1_SELS_3 |                // SMCLK = DCO
            CS_CTL1_SELM_3;                 // MCLK = DCO
    CS->KEY = 0;                      // Lock CS module from unintended accesses
}

void uart_OLED_GPIO()
{
    P3->DIR |= BIT3;            // Set Pin 3.3 as Tx
    P3->DIR &= ~BIT2;           // Set Pin 3.2 as Rx (Not used here, but whatever)
    P3->OUT |= BIT3;
    P3->SEL0 |= BIT2 | BIT3;
    P3->SEL1 = 0;
}

void uart_Arduino_GPIO()
{
    P9->DIR |= BIT7;            // Set Pin 9.7 as Tx
    P9->DIR &= ~BIT6;           // Set Pin 9.6 as Rx
    P9->OUT |= BIT7;
    P9->SEL0 |= BIT6 | BIT7;
    P9->SEL1 = 0;
    P9->REN &= ~BIT6;
}
