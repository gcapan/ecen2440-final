/*
 * gpio.c
 *
 *  Created on: Oct 14, 2019
 *      Author: grant
 */
#include "msp.h"

void config_drv_gpio(void)
{
    /*P1->DIR |= BIT6 | BIT7;
    P1->OUT |= BIT6 | BIT7;
    P1->SEL0 = 0;
    P1->SEL1 = 0;*/

    /*P8->DIR |= 0b00011111;
    P8->OUT |= 0b00010001;      // set the enable and vcc high
    P8->SEL0 = 0;
    P8->SEL1 = 0;*/

    P2->DIR |= BIT3 | BIT4;      // configure pin 3 of port 2 as pwm output
    P2->SEL0 = 0b10111;
    P2->SEL1 = 0;

    P5->SEL0 = 0;
    P5->SEL1 = 0;
    P5->DIR |= BIT0;
    P5->OUT |= BIT0; // enable pin

    /*P5->DIR |= BIT1; // VCC
    P5->OUT |= BIT1;*/

    //P5->DIR |= BIT2; //
    //P5->OUT |= BIT2;


}


